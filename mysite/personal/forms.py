from django import forms

class NameForm(forms.Form):
    post = forms.CharField(widget = forms.TextInput(
        attrs = {
            'class':'send_email_form form-container form-container-transparent form-container-white',
             'style':'width:50% ; float:left;',
            'placeholder': 'Type text Here...',
            'maxlength':1000000
        },
    ),required = False)
    tag = forms.CharField(widget = forms.TextInput(
        attrs = {
            'class':'send_email_form form-container form-container-transparent form-container-white',
             'style':'width:50% ; float:left;',
            'placeholder': 'Search tag Here...',
            'maxlength':1000000
        },
    ),required = False)
